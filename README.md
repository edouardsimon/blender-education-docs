# Blender Education Documentation
Welcome to the Blender Education Documentation.


## Building this Documentation Offline

The following assumes [Python 3](https://www.python.org/),
[pip](https://pypi.org/project/pip/), [Git](https://git-scm.com/) and
[Git LFS](https://git-lfs.com/) are installed.

**Set up Git LFS for the current user:**
```sh
git lfs install
```

**Clone the documentation sources:**
```sh
git clone https://projects.blender.org/blender/blender-education-docs.git education-docs
```
This will clone the sources into a `developer-docs` directory inside the current
one.

**Install all dependencies, such as
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/):**
```sh
python -m pip install -r requirements.txt
```

**Build this documentation with live reloading:**
```sh
mkdocs serve or python -m mkdocs serve
```

Alternatively `mkdocs build` will generate the documentation as HTML into a
`site/` directory. Simply open `site/index.html` in a browser.

## Links
TODO