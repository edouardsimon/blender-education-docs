---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Blender Badges

Developing a Universal Blender Curriculum

Contributors:
