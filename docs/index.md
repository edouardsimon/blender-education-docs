---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Blender Education Documentation

<div class="grid cards" markdown>
